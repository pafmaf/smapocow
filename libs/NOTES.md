
# Lib Sources
-------------------

## libui - portable GUI for C

* https://github.com/andlabs/libui (MIT)

```
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=RELEASE -DBUILD_SHARED_LIBS=OFF -G "Visual Studio 14 2015 Win64"
cmake --build .
```

Doesn't quite work, you need to set the 64 bits properly in CMake and resolve
some issues in VS manually.
