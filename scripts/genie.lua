
solution "Examples"
    configurations  { "Debug", "Release" }
    platforms       { "x64" }
    flags           { "FatalWarnings", "Optimize" }
    language        "C++"
    location        "../build"
    objdir          "../build/obj"

    project "ui_histogram"
        kind "WindowedApp"
        targetdir		"../bin"
        links           { "libui" }
        files           { "../src/libui/histogram.cpp" }
        includedirs     { "../contrib" }
        libdirs         { "../libs/**" }

    project "ui_gallery"
        kind "WindowedApp"
        targetdir		"../bin"
        links           { "libui" }
        files           { "../src/libui/gallery.cpp" }
        includedirs     { "../contrib" }
        libdirs         { "../libs/**" }

    project "sdl_basic"
        kind "WindowedApp"
        targetdir		"../bin"
        links           {
            "SDL2main",
            "SDL2",
            "imm32",
            "ole32",
            "oleaut32",
            "winmm",
            "glbinding",
            "boost_log",
            "boost_system",
            "boost_chrono",
            "boost_thread",
            "opengl32",
            "mingw32",
            "version"
        }
        files           { "../src/sdl/basic.cpp" }
        libdirs         { "../libs/**" }

newaction {
    trigger     = "install",
    description = "Copy necessary libs to bin dir.",
    execute     = function()
        os.copyfile("../libs/x64/libui/libui.dll", "../bin/libui.dll")
    end
}

newaction {
    trigger     = "clean",
    description = "Clean up build files.",
    execute     = function ()
        os.rmdir("../build")
        os.rmdir("../bin")
    end
}
