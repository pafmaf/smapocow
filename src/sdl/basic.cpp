
#include <stdio.h>

#include "SDL2/SDL.h"
#include <glbinding/gl/gl.h>
#include <glbinding/Binding.h>
using namespace gl;

SDL_Window *window;
void draw_loop();

int WinMain() {
  SDL_DisplayMode mode;

  int32_t width = 640;
  int32_t height = 480;
  window = SDL_CreateWindow("SDL2/OpenGL Demo", 0, 0, width, height,
                            SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

  SDL_GetDisplayMode(0, 0, &mode);
  SDL_SetWindowPosition(window, mode.w/2-width/2, mode.h/2-height/2);

  SDL_GLContext glcontext = SDL_GL_CreateContext(window);

  glbinding::Binding::initialize();

  glBegin(GL_TRIANGLES);
  glClearColor(0, 0, 0, 1);
  glClear(GL_COLOR_BUFFER_BIT);
  glEnd();
  SDL_GL_SwapWindow(window);

  draw_loop();

  SDL_GL_DeleteContext(glcontext);
}

void draw_loop() {
  bool loop = true;

  while (loop) {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
      if (event.type == SDL_QUIT)
        loop = false;

      if (event.type == SDL_KEYDOWN) {
        switch (event.key.keysym.sym) {
        case SDLK_ESCAPE:
          loop = false;
          break;
        case SDLK_r:
          glClearColor(0.3, 0.0, 0.0, 1.0);
          glClear(GL_COLOR_BUFFER_BIT);
          SDL_GL_SwapWindow(window);
          break;
        case SDLK_g:
          glClearColor(0.0, 0.3, 0.0, 1.0);
          glClear(GL_COLOR_BUFFER_BIT);
          SDL_GL_SwapWindow(window);
          break;
        case SDLK_b:
          glClearColor(0.0, 0.0, 0.3, 1.0);
          glClear(GL_COLOR_BUFFER_BIT);
          SDL_GL_SwapWindow(window);
          break;
        default:
          break;
        }
      }
    }
  }
}
