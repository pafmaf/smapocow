@echo off

if not exist "tmp" (
    md tmp
)

if not exist "%~dp0scripts\genie.exe" (
	echo Downloading GENie
	bitsadmin /transfer "genie" /priority FOREGROUND "https://github.com/bkaradzic/bx/raw/master/tools/bin/windows/genie.exe" "%~dp0\scripts\genie.exe"
)

if not exist "%~dp0tmp\mingw.exe" (
    echo Downloading MinGW Dist
    bitsadmin /transfer "mingw" /priority FOREGROUND "https://nuwen.net/files/mingw/mingw-14.0-without-git.exe" "%~dp0tmp\mingw.exe"
)

if not exist "%~dp0mingw-base" (
    echo Unpacking MinGW
    %~dp0\tmp\mingw.exe -y -o"%~dp0\mingw-base"
)

if not exist "%~dp0tmp\libui.zip" (
    echo Downloading libui
    bitsadmin /transfer "libui" /priority FOREGROUND "https://github.com/andlabs/libui/archive/d93bb2c48f0c6a521c8dd089ab84c73977ce3697.zip" "%~dp0\tmp\libui.zip"
)

if not exist "%~dp0contrib\libui" (
    echo Setting up libui
    call %~dp0mingw-base\MinGW\set_distro_paths.bat

    7z x %~dp0tmp\libui.zip -o%~dp0\contrib\
    move %~dp0contrib\libui-* %~dp0contrib\libui
)

echo ### Probably Done?
