
Small, Portable C++ Compiler Env For Windows _(smapocow)_
---------------------------------------------------------

## Description

**Smapocow** is a small, portable, easy-to-use wireframe project, enabling compilation of C++ projects on Windows machines that lack the ability to compile C source entirely. E.g. don't have a proper Cygwin, MinGW, Visual Studio, (...) installation yet. (No quick `g++ main.cpp`.)

  A few clicks on Windows `.bat` files will extract a minimal MinGW distro into a local directory and compile a few example GUI applications for you!  

  Downloads combined are around _50MB_ in size, but will extract to around 500MB+ MinGW stuff.

## Features

This repo is based on a few awesome projects that live on the Internet:

 * 40MB [Nuwen MinGW Distro](https://nuwen.net/mingw.html) - prebuilt MinGW with GCC6, binutils & mingw-w64 (plus 7zip etc.)
     - Essential Build tools
     - Boost, FreeType, glbinding, libjpeg, libpng, SDL2, zlib, ...
 * 1MB [libui](https://github.com/andlabs/libui) - nice, portable GUI lib for C  _(MIT)_
 * 0.5MB [genie](https://github.com/bkaradzic/GENie) - project generator based on lua config files


## How

* Clone this repo or download the `.zip` file
* double-click on `setup.bat`
    - this will download MinGW & setup the necessary libs for you
* double-click on `build.bat`
    - this will build a few example _libui_ `.exe`s into the `bin/` directory

You don't need to get the git submodules yourself, since we can't expect Git to be installed, but you could.

When you're done with your 'mock-up', you can generate project files for a different environment/IDE, e.g. to continue work with MSVS or a proper MinGW installation or port to other platforms, since GENie (see it as CMake if you don't know it) is portable.


### Build Process

[GENie](https://github.com/bkaradzic/GENie) will generate your project files based on the `scripts/genie.lua` project description file. (Port of _Premake5_)

`genie.exe gmake` - will generate proper GNU makefiles to `build/` (`build.bat` default).

Then GNU `make` takes over and builds the binaries.

If you have more `.dll`s you want to link to, put them into `libs/...` accordingly and edit `scripts/genie.lua` so that
 * it finds both the _libs_..
 * ..and the _include_-sources
 * it _copies the DDLs_ over to the `bin/` folder

(Static compilation seems a little problematic.)

Check GENie and Premake docs & references for more info.

## Why

Setting up any build env on Windows is an interesting process. Having a quick option to start programming on a fresh Windows installation and compiling a simple GUI application right away was my main goal here.

No `sudo apt-get <binutils>`, no `brew install <binutils>` and no quick way of installing 3rd party tools, e.g. to extract `.tar.gz`...

This project is probably useful when:
* you can't be bothered to setup a proper build env
* http://cpp.sh/ is not enough to quickly try sth.
* waiting for Visual Studio to install is not an option
* you don't have permissions on a Windows machine to install VS etc.
* you've never compiled stuff on Windows before
